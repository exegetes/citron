<?php

if (!stream_resolve_include_path('../conf/cocktail.php'))
	response(["Configuration file '../conf/cocktail.php' not found.\nSample configuration file can be found in '/conf/cocktail.php.smp'"]);

require '../conf/cocktail.php';

$errors = [];

if (!isset($_REQUEST['action']))
	$errors[] = "No 'action' parameter given, dunno what to do";

if (!empty($errors))
	response($errors);


switch ($_REQUEST['action']) {
	case 'shake':
		init_check();
		shake();
		break;
	
	case 'status':
		init_check();
		status();
		break;
}


function init_check () {
	global $COCKTAIL;
	if (!isset($COCKTAIL['cocktail']['binary'])) {
		$errors[] = "'cocktail:binary' n'est pas configuré dans cocktail.php";
		response($errors);
	}
}


function shake() {
	global $COCKTAIL;
	$params = $_REQUEST;
	$errors = [];
	
	foreach (['dossier','projetId'] as $p)
		if (!preg_match('/^[\W\d-]+$/i',$params[$p]))
			$errors[] = "'$p' parameter invalid";
	
	foreach (['projetPadPrincipal','projetPadGarde'] as $p)
		if (!preg_match('_^https?://pad\.exegetes\.eu\.org/_i',$params[$p]))
			$errors[] = "'$p' parameter invalid ('{$params[$p]}')";
	
	if (!isset($COCKTAIL['cocktail'])) {
		$errors[] = "'cocktail' n'est pas configuré dans cocktail.php";
		response($errors);
	}
	
	$cocktailbin = $COCKTAIL['cocktail']['binary'];
	if (!is_executable($cocktailbin))
		$errors[] = "'cocktail:binary' '$cocktailbin' n'est pas exécutable";
	
	if (!empty($errors))
		response($errors);
	
	foreach (['projetPadPrincipal','projetPadGarde'] as $p)
		$params[$p] = urlencode($params[$p]);
	
	// Shell-escape parameters
	foreach (['projetPadPrincipal','projetPadGarde','dossier','projetId'] as $p)
		$params[$p] = escapeshellarg($params[$p]);
	
//	$nowait = '> /dev/null 2>&1 &';
	$cmd = "$cocktailbin -d {$params['dossier']} -b {$params['projetPadPrincipal']} -g {$params['projetPadGarde']} -p {$params['projetId']}";
	
	exec($cmd,$output);
	response([],array_merge(['Commande :','$cmd','Output:'],$output));
}


function status () {
	global $COCKTAIL;
	$params = $_REQUEST;
	$errors = [];
	
	if (!isset($COCKTAIL['cocktail']['store']))
		$errors[] = "'cocktail:store' n'est pas configuré dans config.yml";
	
	foreach (['dossier','projetId'] as $p)
		if (!preg_match('/^[\W\d-]+$/i',$params[$p]))
			$errors[] = "'$p' parameter invalid";
	
	if (!empty($errors))
		response($errors);
	
	$lock_filename = "{$COCKTAIL['cocktail']['store']}/{$params['dossier']}/{$params['projetId']}.lock";
	$pdf_filename = "{$COCKTAIL['cocktail']['store']}/{$params['dossier']}/{$params['projetId']}.pdf";
	
	if (is_file($lock_filename))
		$res = [
			'status' => 'IN_PROGRESS',
			'time' => format_time(stat($lock_filename)['mtime']),
		];
	else if (is_file($pdf_filename))
		$res = [
			'status' => 'COMPLETED',
			'time' => format_time(stat($pdf_filename)['mtime']),
		];
	else
		$res = [
			'status' => 'NONE',
			'time' => 'Aucune compilation.',
		];
	
	header ('Content-Type: application/json');
	echo json_encode($res);
	die();
}


function response ($errors=[],$output=[]) {
	header ('Content-Type: application/json');
	
	echo json_encode([
		'status' => empty($errors) ? 'success' : 'error',
		'output' => $output,
		'errors'=> $errors,
	]);
	die();
}


function format_time ($unixtime) {
	return is_numeric($unixtime) ? date('d/m/Y H:i:s',$unixtime) : $unixtime;
}


?>