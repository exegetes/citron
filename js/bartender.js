

$(document).delegate( '.js-bartend', 'click', function () {
	
	$.ajax({
		url: '/ajax/bartender.php',
//		type: 'post',
		data: {
			action: 'shake',
			dossier: $(this).data('dossier'),
			projetId: $(this).data('projet-id'),
			projetPadPrincipal: $(this).data('projet-pad-principal'),
			projetPadGarde: $(this).data('projet-pad-garde')
		},
		success: function (data) {
			if (data.status === 'error')
				alert('Bartender says: '+data.errors.join(' '));
			else
				setTimeout(status,1000);
		},
		error: function () {
			alert("Je n'ai pas pu contacter le barman pour presser le projet...");
		}
	});
	
	return false;
});


function status () {
	var deferreds = [];
	var need_refresh = 0;
	
	$('.js-status').each( function () {
		var statusdiv = $(this);
		
		deferreds.push($.ajax({
			url: '/ajax/bartender.php',
			data: {
				action: 'status',
				dossier: statusdiv.data('dossier'),
				projetId: statusdiv.data('projet-id')
			},
			success: function (data) {
				if (data.status === 'error') {
					alert('Bartender says: '+data.errors.join(' '));
				}
				
				statusdiv.find('.js-status-placeholder').text(data.status);
				statusdiv.find('.js-time-placeholder').text(data.time);
				
				if (data.status === 'IN_PROGRESS')  // on relance la vérif de statut dans 3s pour voir si la compil est terminée
					need_refresh++;
			},
			error: function () {
				alert("Je n'ai pas pu contacter le barman pour le statut de compilation...");
			}
		}));
	});
	
	$.when.apply($,deferreds).done( function () {
		if (need_refresh > 0)
			setTimeout(status,3000);
	});
}
$(document).ready( function () {
	status();
});