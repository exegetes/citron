<?php
include_once 'templates/tpl-main-top.php';
?>

<body id="view-project-<?php echo $o['rowid']; ?>" class="page-view-project">
	
	<script type="text/javascript" src="/js/bartender.js"></script>
	
    <h1 class="app titre">
      <a href="/voir.php?type=dossier&id=<?php echo $o['dossier_id']; ?>" class="dossier" id="<?php echo $o['dossier']; ?>"><?php echo $o['dossier']; ?></a> / 
    <?php echo $o['name']; ?>
    </h1>

    <div class="métadonnées">
	    <a href="editer.php?type=project&id=<?php echo $o['rowid']; ?>" type=submit" class="bouton">
		    Éditer
	    </a>
    </div>


    <div class="voir">
	<form action="/bartender/shake" method="get">
	<div class="form-group hidden">
	    <label for="projetId">Identifiant:</label>
	    <input type="text" class="form-control" id="projetId" name="projetId" value="<?php echo $o['rowid']; ?>" placeholder="">
	</div>
	<div class="form-group">
	    <label for="projetPadPrincipal">Pad principal:</label>
	    <input type="url" class="form-control hidden" id="projetPadPrincipal" name="projetPadPrincipal" placeholder="https://pad.exegetes.eu.org/" value="<?php echo $o['main_pad']; ?>">
        <a href="<?php echo $o['main_pad']; ?>"><?php echo $o['main_pad']; ?></a>
	</div>
	<div class="form-group">
	    <label for="projetPadGarde">Page de garde:</label>
	    <input type="url" class="form-control hidden" id="projetPadGarde" name="projetPadGarde" placeholder="https://pad.exegetes.eu.org/" value="<?php echo $o['cover_pad']; ?>">
        <a href="<?php echo $o['cover_pad']; ?>"><?php echo $o['cover_pad']; ?></a>
	</div>
	<div class="form-group hidden">
	    <label for="projetPadAutre1">Autre pad: <a href="#">Ajouter</a></label>
	    <input type="url" class="form-control hidden" id="projetPadAutre1" name="projetPadAutre1" placeholder="https://pad.exegetes.eu.org/">
	</div>
	<div class="form-group hidden">
	    <label for="projetDossier">Dossier: <a href="ajout-dossier.html">(Nouveau dossier ?)</a></label>
	    <select name="dossier" id="dossiers" multiple="" class="form-control input-lg select2 select2-offscreen" tabindex="-1">
		    		<?php foreach ($dossiers as $d) { ?>
					<option value="<?php echo $d['rowid']; ?>"
						<?php
							if ($d['rowid'] === $o['dossier_id'])
								echo " selected ";
						?>
					>
						<?php echo $d['name']; ?>
					</option>
				<?php } ?>
	    </select>
	</div>

	<div class="actions">
		
		<button type="submit" class="bouton presse">Presser</button>
		
		<!-- nouveau bouton de presse -->
		<div style="display: none">
	    <button class="bouton presse js-bartend" type="button"
				data-projet-pad-garde="<?php echo $o['cover_pad']; ?>"
				data-projet-pad-principal="<?php echo $o['main_pad']; ?>"
				data-projet-id="<?php echo $o['rowid']; ?>"
				data-dossier="<?php echo $o['dossier_id']; ?>"
				>
				Presser
			</button>
		</div>
	</div>

    <div class="ressources">
    <span>Télécharger</span>
      <a class="pdf" href="/beta/<?php echo $o['dossier_id']; ?>/<?php echo $o['rowid']; ?>.pdf">PDF</a>
      <a class="docx" href="/beta/<?php echo $o['dossier_id']; ?>/<?php echo $o['rowid']; ?>.docx">Docx</a>
      <a class="html5" href="/beta/<?php echo $o['dossier_id']; ?>/<?php echo $o['rowid']; ?>.html">HTML</a>
      <a class="txt" href="/beta/<?php echo $o['dossier_id']; ?>/<?php echo $o['rowid']; ?>.txt">Texte</a>
			<div class="js-status"
				data-projet-id="<?php echo $o['rowid']; ?>"
				data-dossier="<?php echo $o['dossier_id']; ?>"
				>
				Dernière compilation&nbsp;:
				<span class="js-status-placeholder">En attente du statut de compilation...</span>
				<span class="js-time-placeholder"></span>
			</div>

    </div>
		
	</form>
		</div>

<?php
include_once 'templates/tpl-main-bottom.php';
?>
